FROM node:10

COPY . /app

WORKDIR /app

RUN npm install && \
    npm run build

CMD ["node", "/app/dist/bundle.js"]
