const express = require("express");
var os = require("os");

const app = express();

app.use(express.json());

app.get("/", (req, res) => {

    const greeting = process.env.GREETING || "Hello";
    const name = req.query.name || "world";

    const message = {
        content: `${greeting} ${name}!`,
        host: os.hostname(),
        time: new Date().getTime()
    };

    res.json(message);
});

module.exports = app;
